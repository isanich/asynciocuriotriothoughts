import asyncio
from types import coroutine


# не путайте классическое объявление корутины с yield в async def функции (асинхронный генератор)
@coroutine
def cgen():
    print('Первый send(None) в цикле asyncio, который изначально пришел в main. Генератор запускается до первого yield')
    rc = yield  # генератор инициализирован, yield поместил None в выходной слот, контекст передан eternal_task
    # cgen сразу готов к следующему send(None) и помещается в очередь

    print(f'Второй send(None) в цикле asyncio. Во входной слот генератора помещен None, его получает "rc".')
    rc = yield rc  # конец второго вызова, yield поместил rc в выходной слот, контекст передан eternal_task
    # rc может быть только None тут, т.к. в остальных случаях будет
    # RunTimeError для coroutine (но не для обычного генератора)

    print(f'Третий send(None) в цикле asyncio. Во входной слот генератора помещен None, его опять получает "rc".')
    yield from asyncio.sleep(0)
    # yield from asyncio.sleep(0) фактически равносильно пустому yield (для объявления через @coroutine),
    # т.к. внутри sleep как раз пустой yield для delay=0 и контекст будет передан другому eternal_task

    return rc  # StopIteration co значением в return, возвращается результат
    # eternal_task так и продолжит работу (пока asyncio.run не закроет цикл)


async def eternal_task():
    while True:
        print('"eternal_task" iteration.')
        await asyncio.sleep(0)  # отпускаем контекст, он вернется к cgen, т.к. это единственный таск и он готов


async def main():
    print('Main получает send(None) из цикла asyncio.')
    asyncio.create_task(eternal_task())  # создается таск, но контекст остается у main
    # await cgen() равносильно yield from cgen() для @coroutine объявления
    return await cgen()  # переходим в cgen, в нем передается контекст за счет первого же yield
    # await НЕ передает контекст сам по себе

coro = main()
# asyncio.run создает Task и кладет в очередь Event Loop
# цикл будет работать до его завершения или ошибки которая поднимется из main
res = asyncio.run(coro)
