# Введение

Начну с общих вещей для полноты картины. Корутины работают на основе генераторов, которые благодаря `send` и `yield` (ну и не надо забывать `yield from`) играют роль сопрограмм, отдельных потоков выполнения.
```python
def gen():
    # g.send(None), запуск до первого yield, инициализация
    yield  # None помещен в выходной слот генератора
    # g.send(None), запуск до rc = yield 0,
    rc = yield 0  # 0 помещен в выходной слот генератора
    # g.send(1) во входной слот генератора помещено значение, которое получает rc
    print(rc)  # 1
    return rc


g = gen()
try:
    g.send(None)  # yield вернул None
    g.send(None)  # yield вернул 0
    g.send(1)
except StopIteration as e:
    print(e.value)  # 1
```
В отличи от генератора выше, запуском корутин занимается Event Loop. Для корутин можно делать send (None) и вручную, но смысла мало, т.к. IO Polling, ожидание и переключение на другой контекст тоже придется делать самому. Ниже представлен простейший пример Event Loop с переключением контекста [из документации curio](https://github.com/isanich).
```python
import time
from collections import deque
from types import coroutine


@coroutine
def switch():
    yield ('switch',)


@coroutine
def sleep(seconds):
    yield ('sleep', seconds)


tasks = deque()
sleeping = []


def run():
    while tasks:
        coro = tasks.popleft()
        try:
            request, *args = coro.send(None)
            if request == 'switch':
                tasks.append(coro)
            elif request == 'sleep':
                seconds = args[0]
                deadline = time.time() + seconds
                insort(sleeping, (deadline, coro))
            else:
                print('Unknown request:', request)
        except StopIteration as e:
            print('Task done:', coro)

        while not tasks and sleeping:
            now = time.time()
            duration = sleeping[0][0] - now
            if duration > 0:
                time.sleep(duration)
            _, coro = sleeping.pop(0)
            tasks.append(coro)


async def countdown(n):
    while n > 0:
        print('T-minus', n)
        await switch()
        n -= 1


async def countup(stop):
    n = 1
    while n <= stop:
        print('Up we go', n)
        await switch()
        n += 1

tasks.append(countdown(10))
tasks.append(countup(15))
run()

# T-minus 10
# Up we go 1
# T-minus 9
# Up we go 2
...
```
[Еще пример Event Loop с нуля](https://github.com/AndreLouisCaron/a-tale-of-event-loops), чуть более сложный. В asyncio принцип работы примерно такой же, ниже приведен пример с комментариями.
```python
import asyncio
from types import coroutine


# не путайте классическое объявление корутины с yield в async def функции (асинхронный генератор)
@coroutine
def cgen():
    print('Первый send(None) в цикле asyncio, который изначально пришел в main. Генератор запускается до первого yield')
    rc = yield  # генератор инициализирован, yield поместил None в выходной слот, контекст передан eternal_task
    # cgen сразу готов к следующему send(None) и помещается в очередь

    print(f'Второй send(None) в цикле asyncio. Во входной слот генератора помещен None, его получает "rc".')
    rc = yield rc  # конец второго вызова, yield поместил rc в выходной слот, контекст передан eternal_task
    # rc может быть только None тут, т.к. в остальных случаях будет
    # RunTimeError для coroutine (но не для обычного генератора)

    print(f'Третий send(None) в цикле asyncio. Во входной слот генератора помещен None, его опять получает "rc".')
    yield from asyncio.sleep(0) 
    # yield from asyncio.sleep(0) фактически равносильно пустому yield (для объявления через @coroutine),
    # т.к. внутри sleep как раз пустой yield для delay=0 и контекст будет передан другому eternal_task

    return rc  # StopIteration co значением в return, возвращается результат
    # eternal_task так и продолжит работу (пока asyncio.run не закроет цикл)


async def eternal_task():
    while True:
        print('"eternal_task" iteration.')
        await asyncio.sleep(0)  # отпускаем контекст, он вернется к cgen, т.к. это единственный таск и он готов


async def main():
    print('Main получает send(None) из цикла asyncio.')
    asyncio.create_task(eternal_task())  # создается таск, но контекст остается у main
    # await cgen() равносильно yield from cgen() для @coroutine объявления
    return await cgen()  # переходим в cgen, в нем передается контекст за счет первого же yield
    # await НЕ передает контекст сам по себе

coro = main()
# asyncio.run создает Task и кладет в очередь Event Loop
# цикл будет работать до его завершения или ошибки которая поднимется из main
res = asyncio.run(coro)
```
В коде выше контекст передается по очереди между `cgen` и `eternal_task`(из-за отсутствия IO и `asyncio.sleep(0)` в `eternal_task`). Однако у подхода, основанного на корутинах и Event Loop, есть несколько проблем.

# Общие проблемы
### 1. Передача контекста
В тредах захват GIL (контекста) может происходить [при IO или по истечению 100 "тиков"](http://www.dabeaz.com/python/UnderstandingGIL.pdf) ("тики" примерно равны инструкциям интерпретатора, возможно, что-то уже успело измениться), супервайзором является планировщик OS. В asyncio/curio/trio передача контекста на IO тоже неплохо работает (и позволяет держать значительно большую нагрузку, чем треды), ожиданием IO и переключением тасков занимается Event Loop (например, его [selector](https://docs.python.org/3/library/selectors.html) версия). Однако из-за тяжелых CPU операций или блокирующего IO в таске весь Event Loop может легко "заглохнуть" (таск держит контекст, не возвращая его долгое время), и await (как, к сожалению, думают некоторые разработчики) тут не спасет. Общим подходом можно считать передачу контекста вручную (как в `eternal_task` выше) или запуск таких тасков в отдельных тредах/процессах. Стоит еще учесть,что для CPU-bound задач треды подходят хуже процессов не только из-за GIL (проблема с [CPU-bound threads tend to “starve out” IO-bound](https://bugs.python.org/issue7946)). Ниже особенности и возможности в разных библиотеках.
#### Asyncio
* Передать контекст явно в asyncio можно через пустой yield в `@coroutine` style корутине или через `asyncio.sleep(0)` в async def версии (тот же пустой yield внутри).
* В отличии от trio не все стандартные библиотечные методы с await передают контекст.
* Для запуска в отдельном треде или процессе присутствует метод [loop.run_in_executor](https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.run_in_executor), который использует [concurrent.futures](https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.Executor) executor'ы.
* Взаимодействие с Event Loop в main thread возможно через [asyncio.run_coroutine_threadsafe](https://docs.python.org/3/library/asyncio-task.html#asyncio.run_coroutine_threadsafe) и [loop.call_soon_threadsafe](https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.call_soon_threadsafe).
* Стоит отметить удобные [asyncio.create_subprocess_exec](https://docs.python.org/3/library/asyncio-subprocess.html#asyncio.create_subprocess_exec) и [asyncio.create_subprocess_shell](https://docs.python.org/3/library/asyncio-subprocess.html#asyncio.create_subprocess_shell), которые позволяют работать с PIPE и сигналами.
#### Curio
* `curio.sleep(0)` работает как и в asyncio.
* Как и в asyncio, не все библиотечные методы с await передают контекст, о чем [пишет](https://trio.readthedocs.io/en/latest/design.html#cancel-points-and-schedule-points) Nathaniel J. Smith автор trio.
* В curio не используются по умолчанию executor'ы из `concurrent.futures`, [curio.workers](https://curio.readthedocs.io/en/latest/reference.html#module-curio.workers) для запуска тредов и процессов реализованы автором.

    > These functions have no external library dependencies, have substantially less communication overhead, and more predictable cancellation semantics.
    > — David Beazley

* Есть свой [subprocess wrapper module](https://curio.readthedocs.io/en/latest/reference.html#module-curio.subprocess) c аналогичными asyncio возможностями.
* [Работа с сигналами](https://curio.readthedocs.io/en/latest/reference.html#signals) сделана удобней, чем в asyncio и trio (там еще в процессе).
* Есть [свой модуль](https://curio.readthedocs.io/en/latest/reference.html#module-curio.file) для работы с файлами (в отличие от asyncio, [aiofiles](https://github.com/Tinche/aiofiles) - последний раз, когда я смотрел, были написаны через тред экзекутор).
* Реализованы [каналы](https://curio.readthedocs.io/en/latest/reference.html#module-curio.channel) для взаимодействия с curio запущенным в другом процессе (через сокеты).
* **Значительно больше инструментов чем в asyncio и trio** для взаимодействия тредов и асинхронного кода:
    1. [curio.abide ](https://curio.readthedocs.io/en/latest/reference.html#abide) позволяет "ждать" любые awaitable объекты из curio напрямую в стандартном Python треде.
    2. [Asynchronous Threads](https://curio.readthedocs.io/en/latest/reference.html#asynchronous-threads), которые удобно создаются через декоратор `@async_thread` и работают с awaitable объектами через [AWAIT()](https://curio.readthedocs.io/en/latest/reference.html#AWAIT).
    3. [UniversalQueue](https://curio.readthedocs.io/en/latest/devel.html?highlight=UniversalQueue%20#thread-task-queuing) cо стандартным для тредов и curio тасков интерфейсом.
    4. David Beazley давно уже "собаку съел" на тредах. Стоит посмотреть его последнюю proof of concept библиотеку [threado](https://github.com/dabeaz/thredo), которая была выложена в июле 2018. Не совсем понятно, будет ли развитие, документации мало, но есть [скринкаст](https://www.youtube.com/watch?v=U66KuyD3T0M).
#### Trio
* Автор trio уделяет **особое** внимание правильной передаче контекста. Им введена концепция [checkpoint'ов](https://trio.readthedocs.io/en/latest/reference-core.html#checkpoints), которые кроме scheduling (собственно, передача контекста) являются точкой cancellation: при возврате к чекпойнту проверяются внешние cancel scope и scheduling может не произойти, например, при таймауте одного из них, - это является одной из важных особенностей/преимуществом trio.
* `trio.sleep(0)` - все еще способ передать контекст вручную, кроме него есть возможность вызвать [trio.hazmat.checkpoint](https://trio.readthedocs.io/en/latest/reference-hazmat.html#trio.hazmat.checkpoint).
* Все библиотечные методы с await являются чекпойнтами, а те, которые не являются, сделаны синхронными специально.
* Asyncio и Curio (как минимум раньше) придерживаются базовой round-robin scheduling strategy, из-за чего некоторые таски могут перетягивать на себя контекст чаще других и мешать им. В trio на данный момент используется FIFO с батчами тасков (из которых таски достаются в случайном порядке), это не решает проблему, но может помочь в некоторых случаях.
    > Curio also had a number of issues like this before I fixed them. Trio avoids this by dequeuing tasks in batches: first it collects up everything that's ready to run right now, then it runs all those, and then only after it's finished that batch does it go back and look for more things to run. So if tasks 1, 2, 3 reschedule themselves immediately, that's fine, they get put in the queue, but we don't look at that queue until after we've finished running task 4 and go back to create our next batch.
    > — Nathaniel J. Smith
* Кроме этого, в trio [ищут альтернативную концепцию](https://github.com/python-trio/trio/issues/32) для планирования переключения тасков.
* Как и в curio есть свой [trio.run_sync_in_worker_thread](https://trio.readthedocs.io/en/latest/reference-core.html#trio.run_sync_in_worker_thread) для запуска тасков в отдельном треде и [гибкая настройка](https://trio.readthedocs.io/en/latest/reference-core.html#trio-s-philosophy-about-managing-worker-threads) спавна новых тредов.
* Реализовано взаимодействие тред-воркеров с trio тредом для запуска синхронных и асинхронных функций через [trio.BlockingTrioPortal](https://trio.readthedocs.io/en/latest/reference-core.html#trio.BlockingTrioPortal). Также можно использовать [run_sync_soon](https://trio.readthedocs.io/en/latest/reference-hazmat.html#trio.hazmat.TrioToken.run_sync_soon).
* В трио, также как и в curio, [реализован свой модуль](https://trio.readthedocs.io/en/latest/reference-io.html?highlight=file#asynchronous-file-objects) для асинхронной работы с файлами.
* Trio поддерживает PyPy, где более адекватно работают треды, в целом (распределение времени CPU).
* Работа с subprocess пока не реализована.

### 2. Синхронизация, взаимодействие и отмена тасков
Классические проблемы тредов, такие как Deadlock и Race Condition, вполне характерны и для корутин. Кроме этого необходимы возможности для контроля нескольких запущенных тасков и гибкой настройки таймаутов, а также возможность иметь локальные для таска переменные. Рассмотрим, что могут предоставить разные библиотеки.

Начнем с того, что и [у asyncio](https://docs.python.org/3/library/asyncio-sync.html#synchronization-primitives) и [у curio](https://curio.readthedocs.io/en/latest/reference.html#synchronization-primitives) и [у trio](https://trio.readthedocs.io/en/latest/reference-core.html#broadcasting-an-event-with-event) есть стандартные примитивы синхронизации, такие как семафоры и ивенты. Кроме того, во всех библиотеках реализованы очереди для простой синхронизации тасков: [очереди в asyncio](https://docs.python.org/3/library/asyncio-queue.html#queues), [очереди в curio](https://curio.readthedocs.io/en/latest/reference.html#queues), [очереди в трио](https://trio.readthedocs.io/en/latest/reference-core.html#trio.Queue) .

#### Asyncio
* Начиная с Python 3.7 добавлена функция [asyncio.run](https://docs.python.org/3/library/asyncio-task.html#asyncio.run), которая по задумке должна был быть основной точкой входа в приложение, а при его завершении канселить все таски и закрывать асинхронные генераторы (thanks curio).
* В 3.7 добавлена возможность создавать локальные для таска переменные через [contextvars](https://docs.python.org/3/library/contextvars.html#asyncio-support), что очень полезно для создания локального для таска контекста, мониторинга, дебага и т.п.
* Для контроля за несколькими тасками в можно использовать [asyncio.gather](https://docs.python.org/3/library/asyncio-task.html#asyncio.gather), однако есть проблема: если аргумент `return_exceptions` остается `False` по умолчанию, то когда один из тасков падает, его Exception возвращается наверх, но остальные таски продолжают свою работу silently, что может вызвать проблемы. В 3.8 этот инструмент [планируется заменить](https://youtu.be/ReXxO_azV-w?t=27m26s) на `asyncio.TaskGroup`.
* Похожий на `asyncio.gather` (в том числе по малой полезности) метод [asyncio.wait](https://docs.python.org/3/library/asyncio-task.html#asyncio.wait) позволяет ждать группу тасков по определенному условию.
* В 3.8 [возможно будет добавлены ](https://youtu.be/ReXxO_azV-w?t=30m53s) timeout и cancel scopes, а также контекст менеджер для `shield()`. Thanks trio. На данный момент в библиотеке есть только [asyncio.wait_for](https://docs.python.org/3/library/asyncio-task.html#asyncio.wait_for) для таймаута отдельного таска.
#### Curio
* Curio использует [единую точку входа](https://curio.readthedocs.io/en/latest/reference.html#the-kernel) `curio.run` или `curio.Kernel().run()`
* В curio реализованы [Task Groups](https://curio.readthedocs.io/en/latest/reference.html#task-groups): если любой или несколько тасков в группе падают с Exception, возвращается TaskGroupError, которая содержит errors - набор произошедших исключений. Task Group обычно используется как контекст менеджер.
* Для таймаута тасков используется контекст менеджер [timeout_after](https://curio.readthedocs.io/en/latest/reference.html#timeouts) и [ignore_after](https://curio.readthedocs.io/en/latest/reference.html#ignore_after), для Task Groups он автоматически отменит все входящие в нее таски. Есть возможность защищать некоторые таски или группы тасков от отмены инструментами [сancellation сontrol](https://curio.readthedocs.io/en/latest/reference.html#cancellation-control). [Nested Timeouts](https://curio.readthedocs.io/en/latest/devel.html#nested-timeouts) также поддерживаются.
* [Недавно была добавлена](https://github.com/dabeaz/curio/commit/d199b7d780f56177fc416fdcb4738a8bca268854) поддержка сontextvars из стандартной библиотеки 3.7.
#### Trio
* Аналогично curio имеется единая точка входа [trio.run](https://trio.readthedocs.io/en/latest/reference-core.html#trio.run).
* Trio предлагает концепцию запуска тасков только из [т.н. nursery](https://trio.readthedocs.io/en/latest/reference-core.html#nurseries-and-spawning) (из создаваемого контекст менеджером `trio.open_nursery` scope), любое исключение пришедшее из таска, всегда приводит к возвращению в nursery и отмене всех остальных тасков. Контекст менеджер закрывается только в случае успешного завершения всех тасков (или ошибки) и не блокирует. Завершение всех тасков - единственный способ как функция с nurcery может успешно выйти без исключения. В случае ошибок в нескольких тасках одновременно возбуждается [trio.MultiError](https://trio.readthedocs.io/en/latest/reference-core.html#trio.MultiError).
* Для создания тасков используется синхронный метод [nursery.start_soon](https://trio.readthedocs.io/en/latest/reference-core.html#trio.The%20nursery%20interface.start_soon), а также асинхронный [nursery.start](https://trio.readthedocs.io/en/latest/reference-core.html#trio.The%20nursery%20interface.start), который выполняет таск до определенного момента (обычно для инициализации) и возвращается (опционально с некоторой информацией из таска).
* [Task local storage](https://trio.readthedocs.io/en/latest/reference-core.html#task-local-storage) поддерживает идею contextvars даже более ранних, чем 3.7 версиях Python.
* При каждом получении контекста [чекпойнт](https://trio.readthedocs.io/en/latest/reference-core.html#checkpoints) проверяет родительские timeout scope и, если хотя один из них не валиден, поднимается исключение Cancelled (которое всегда нужно нужно пропускать). Как и в curio, поддерживаются [nested timeouts ](https://trio.readthedocs.io/en/latest/reference-core.html#cancellation-semantics). Есть возможности по в [выборочной отмене cancellation](https://trio.readthedocs.io/en/latest/reference-core.html#cancellation-api-details).
* Для создания своих очередей и примитивов синхронизации можно использовать низкоуровневый модуль [trio.hazmat.ParkingLot](https://trio.readthedocs.io/en/latest/reference-hazmat.html#trio.hazmat.ParkingLot).

### 3. Exception propogation
Большинство asyncio библиотек имеют баги с возвращением Exception, даже такие как aiohttp и aiopg, обычно Exception так и не попадают наверх или просто дискардятся в тасках. В свою очередь, curio и особенно trio, предлагают несколько иной подход.
#### Asyncio
* [В 3.8 планируется ](https://youtu.be/ReXxO_azV-w?t=26m54s) `loop.create_supervisor()` - виртуальный Event Loop, который может работать как контекст менеджер и поможет работать с exception propogation и recourse cleanup. Thanks Trio.
* На основе `loop.create_supervisor()` для замены `asyncio.gather` в 3.8 [планируется](https://youtu.be/ReXxO_azV-w?t=27m26s) `asyncio.TaskGroup`, которая будет работать аналогично похожим инструментам в trio и curio, в том числе и с Exception.
* На данный момент точно работающим способом поймать Exception в таске можно считать [Future.add_done_callback](https://docs.python.org/3/library/asyncio-future.html#asyncio.Future.add_done_callback), что работает и для Task (т.к. Task наследует от Future).
* Tакие функции как `asyncio.gather`, `asyncio.wait`, `loop.run_untill_complete` и некоторые другие, возвращают ошибки наверх.
#### Curio
* [Task.join](https://curio.readthedocs.io/en/latest/reference.html#Task.join) и [TaskGroup.join](https://curio.readthedocs.io/en/latest/reference.html#TaskGroup.join) всегда поднимают исключение, заключенное в `curio.TaskError` наверх.
* Любое исключение в таске из `TaskGroup`, который был запущен [TaskGroup.spawn](https://curio.readthedocs.io/en/latest/reference.html#TaskGroup.spawn), понимается наверх и приводит к отмене остальных тасков. Это исключение можно поймать вне scope `TaskGroup` в виде `TaskGroupError`, которая содержит информацию о одном или нескольких исключениях произошедших в тасках.
* Из одиночного [curio.spawn](https://curio.readthedocs.io/en/latest/reference.html#spawn), который никогда не дожидались с помощью `join` исключения не поднимаются и даже могут быть отброшены "в стиле asyncio" с флагом `report_crash=False` у `curio.spawn`.
#### Trio
* В trio [одной из ключевых концепций ](https://trio.readthedocs.io/en/latest/design.html#exceptions-always-propagate) является то, что исключения всегда поднимаются наверх из тасков (приводя к отмене всей nurcery).
* Любое непойманное исключение всегда re-raised в родительском таске, если исключений несколько, то возбуждается  [MultiError](https://trio.readthedocs.io/en/latest/reference-core.html#trio.MultiError).
* Не совсем вопрос самих Exception, но причин, по которым они возникают. У trio, в отличие от curio и asyncio, есть свой большой модуль для тестирования [trio.testing](https://trio.readthedocs.io/en/latest/reference-testing.html#module-trio.testing).



### 4. Task Monitoring и Debug
Несмотря на все возможности, ошибки в асинхронном программировании бывают регулярно, поэтому возможность быстро разобраться в чем дело может быть крайне полезной.
#### Asyncio
* [Возможно, в 3.8 добавится](https://youtu.be/ReXxO_azV-w?t=29m52s) low-level tracing API для низкоуровневого мониторинга тасков.
* `loop.set_debug(True)` или `asyncio.run(debug=True)`, а также переменная окружения PYTHONASYNCIODEBUG, позволяют включить debug-режим asyncio, в котором выводится значительно больше информации об ошибках (например, `asyncio checks for coroutines that were not awaited`). Этот режим может значительно замедлить работу приложения, так что держать его включенным не рекомендуется.
* Можно использовать логгер с именем `asyncio`, чтобы получить низкоуровневые логи Event Loop (в debug-режиме пишется больше).
* Попытаться достать более подробный traceback для таска можно через создание callback'а и чтение поля `_source_traceback`.
* Есть неплохой сторонний модуль [aiomonitor](https://github.com/agronholm/aiomonitor).
#### Curio
* [curio.spawn](https://curio.readthedocs.io/en/latest/reference.html#spawn) и [TaskGroup.spawn](https://curio.readthedocs.io/en/latest/reference.html#TaskGroup.spawn) имеют опцию `report_crash`, которая включена по умолчанию и выводит подробное сообщение об Exception.
* В curio доступен очень удобный модуль `curio.monitor`, который позволяет смотреть статистику запущенных тасков, интерактивно отменять их и запускать команду where (показать, где выполнение таска остановилось на текущей передаче контекста). [Пример использования](https://curio.readthedocs.io/en/latest/tutorial.html#tasks) доступен в документации.
* Таски curio могут предоставить читаемый traceback при ошибке, кроме того, у каждого таска доступен параметр [Task.traceback](https://curio.readthedocs.io/en/latest/reference.html#Task.traceback).
* [curio.debug.schedtrace](https://curio.readthedocs.io/en/latest/reference.html#debugging-and-diagnostics) позволяет писать подробный лог scheduler'а предоставленным логгером из стандартной библиотеки.
* Кроме этого, доступен модуль для низкоуровневого перехвата и реакции на действия scheduler'a [curio.activation](https://curio.readthedocs.io/en/latest/reference.html#module-curio.activation).
#### Trio
* Для низкоуровневого мониторинга передачи контекста можно использовать модуль [trio.abc.Instrument](https://trio.readthedocs.io/en/v0.1.0/reference-core.html?highlight=trio.abc.Instrument#trio.abc.Instrument), неплохой пример использования [доступен в документации](https://trio.readthedocs.io/en/latest/tutorial.html#task-switching-illustrated).
* [Недавно в trio был улучшен](https://vorpus.org/blog/beautiful-tracebacks-in-trio-v070/) traceback для ошибок в тасках.
* Есть функция для получения глобальной статистики [trio.hazmat.current_statistics](https://trio.readthedocs.io/en/latest/reference-hazmat.html#trio.hazmat.current_statistics).

# Итоги

## Trio
[Пост в блоге Nathaniel J. Smith,](https://vorpus.org/blog/some-thoughts-on-asynchronous-api-design-in-a-post-asyncawait-world/) где он провел сравнение curio и asyncio, а так же привел общие концепции, которых по его мнению должны придерживаться подобные библиотеки, во многом повлиял на решение о создании его собственной библиотеки trio. Nathaniel также являлся [одним из заметных контрибьюторов curio](https://github.com/dabeaz/curio/commits?author=njsmith) и, по его словам, предлагал достаточно много изменений в библиотеку, некоторые из них [не были в итоге приняты David Beazley](https://github.com/dabeaz/curio/commit/aadc008bb3b1114acc0b0f0cfc5380f044cf16df).

[Приоритетом trio](https://trio.readthedocs.io/en/latest/design.html#high-level-design-principles) является удобство и надежность (а не echo server RPS, к чему стремятся некоторые вещи на asyncio), такие вещи как [trio.testing](https://trio.readthedocs.io/en/latest/reference-testing.html#module-trio.testing) косвенно подтверждают эту концепцию. [Во многих вещах](https://trio.readthedocs.io/en/latest/design.html#cancel-points-and-schedule-points) автору trio пришлось отойти от curio.
> * The only form of concurrency is the task.
> * Tasks are guaranteed to run to completion.
> * Task spawning is always explicit. No callbacks, no implicit concurrency, no futures/deferreds/promises/other APIs that involve callbacks. All APIs are “causal” except for those that are explicitly used for task spawning.
> * Exceptions are used for error handling; try/finally and with blocks for handling cleanup.
#### Мое мнение о Trio
Библиотека хороша, но еще не готова, более того, на мой взгляд мир Python (который еще "понимает" asyncio) не совсем готов к ней. На github крайне мало проектов (могу выделить неплохой [asks](https://github.com/theelous3/asks)) , зато есть попытки написать общие для trio/curio врапперы [multio](https://github.com/theelous3/multio) и более свежий [hyperio](https://github.com/agronholm/hyperio), а также библиотеку для запуска asyncio кода/библиотек из trio [trio-asyncio](https://github.com/python-trio/trio-asyncio). Но важнее, что Core developer'ы Python прислушиваются к идеям из изначально proof of concept библиотеки trio, что еще раз подтверждает правильность этих concept. Надеюсь, автор сумеет быстро добраться до 1.0 и хорошо прорекламировать проект, иначе его может ждать судьба curio, которое существует давно и также "давно лучше" asyncio, только вот ну не пишут люди на нем.

## Asyncio
Asyncio постепенно "добивает свои детские болячки" и начинает уверенно двигаться в сторону curio/trio (во многом благодаря последней), хотя, вероятно, еще долго останется основанной на callback и chaining Futures. Очень часто появляются интересные проекты, формируется комьюнити разработчиков, все-таки стандартная библиотека делает свое дело. Я считаю, что на asyncio можно и нужно писать хорошо, держа в уме опыт curio/trio, а что-то оттуда можно даже реализовать самому при желании (пример - [aionursery](https://github.com/agronholm/aionursery)). Главное не увлекаться, т.к. в 3.8 многие вещи сами попадут туда. И да, они наконец-то **переписали документацию asyncio**  и собираются улучшать ее дальше (хочется позлорадствовать, вспоминая время, когда я пытался разобраться в ней, но не буду).

## Curio
Моя давняя любовь, лучшая документация из всех трех библиотек (особенно, часть [developing with curio](https://curio.readthedocs.io/en/latest/devel.html)). Хочется использовать ее, попробовать `@async_thread`, [curio.meta](https://curio.readthedocs.io/en/latest/reference.html#module-curio.meta) и другие фишки, но почему-то я всегда пишу на asyncio, да и trio уже наступает на пятки. Возможно, час еще придет. С большим уважением к David Beazley за его вдохновляющий код (как появилось trio, я уже написал выше), а еще рекомендую посмотреть его выступления.


## Стоит или не стоит нам выкинуть asyncio?
Я думаю, что не стоит. Определенные сервисы, где, например, нужно можно много работать с тредами и процессами можно попробовать написать на curio, там есть все для этого. Скоро, вероятно, и в trio появится больше подобных инструментов. Для веб-сервисов слишком много придется делать с нуля для обоих библиотеках, я бы предпочел правильно использовать asyncio (с пониманием опыта trio) и такие библиотеки как aiohttp, тем более, что asyncio меняется и скоро на нем станет намного проще писать хорошо.