import time
from collections import deque
from types import coroutine


@coroutine
def switch():
    yield ('switch',)


@coroutine
def sleep(seconds):
    yield ('sleep', seconds)


tasks = deque()
sleeping = []


def run():
    while tasks:
        coro = tasks.popleft()
        try:
            request, *args = coro.send(None)
            if request == 'switch':
                tasks.append(coro)
            elif request == 'sleep':
                seconds = args[0]
                deadline = time.time() + seconds
                insort(sleeping, (deadline, coro))
            else:
                print('Unknown request:', request)
        except StopIteration as e:
            print('Task done:', coro)

        while not tasks and sleeping:
            now = time.time()
            duration = sleeping[0][0] - now
            if duration > 0:
                time.sleep(duration)
            _, coro = sleeping.pop(0)
            tasks.append(coro)


async def countdown(n):
    while n > 0:
        print('T-minus', n)
        await switch()
        n -= 1


async def countup(stop):
    n = 1
    while n <= stop:
        print('Up we go', n)
        await switch()
        n += 1

tasks.append(countdown(10))
tasks.append(countup(15))
run()