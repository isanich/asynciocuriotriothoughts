def gen():
    # g.send(None), запуск до первого yield, инициализация
    yield  # None помещен в выходной слот генератора
    # g.send(None), запуск до rc = yield 0,
    rc = yield 0  # 0 помещен в выходной слот генератора
    # g.send(1) во входной слот генератора помещено значение, которое получает rc
    print(rc)  # 1
    return rc


g = gen()
try:
    g.send(None)  # yield вернул None
    g.send(None)  # yield вернул 0
    g.send(1)
except StopIteration as e:
    print(e.value)  # 1
